# Knowing how effective an intervention is

Repository for "Knowing how effective an intervention, treatment, or manipulation is and increasing replication rates: accuracy in parameter estimation as a partial solution to the replication crisis" by Peters & Crutzen.

The Open Science Framework repository for this project is at https://osf.io/5ejd8.
